# Robot Command Center 2 
# Pythonファイルを開いて編集した後，実行するためのGUI
# CC BY-SA Yasushi Honda 2023 6/3

import tkinter as tk
from tkinter import StringVar
from tkinter.filedialog import askopenfilename, asksaveasfilename, askdirectory
import subprocess
import re


def new_file():
    global filepath
    global program
    program=entry_prog.get()
    dir=askdirectory()
    filepath=dir+'/' + program

    cmd='gedit ' + filepath +' &'
    pro = subprocess.run(cmd,shell=True)
    entry_prog.delete(0,tk.END)
    entry_prog.insert(0, filepath)

def file_save():
    typ = [("Text Files", "*.txt")]
    filepath = asksaveasfilename(defaultextension="txt",filetypes=typ)
    if not filepath:
        return
    with open(filepath, "w") as save_file:
        text = text_editor.get("1.0", tk.END)
        save_file.write(text)
    root.title(f"Text Editor - {filepath}")

def gedit():
    global filepath,program
    typ = [("Text Files", "*.py")]
    filepath = askopenfilename(defaultextension="py",filetypes=typ)
    program=re.findall('[^/]+.py',filepath, flags=re.IGNORECASE)[0]
    cmd='gedit ' + filepath +' &'
    pro = subprocess.run(cmd,shell=True)
    entry_prog.delete(0,tk.END)
    entry_prog.insert(0, filepath)
    text_editor.insert("1.0", "\n" + program + "を編集します．\n")
    #print(program[0])

def open_gvim():
    global filepath,program
    typ = [("Text Files", "*.py")]
    filepath = askopenfilename(defaultextension="py",filetypes=typ)
    program=re.findall('[^/]+.py',filepath, flags=re.IGNORECASE)[0]
    cmd='gvim -geometry +300+300 ' + filepath + ' '
    pro = subprocess.run(cmd,shell=True)
    entry_prog.delete(0,tk.END)
    entry_prog.insert(0, filepath)
    text_editor.insert("1.0", filepath + "を開きます．\n\n")

def execute():
    cmd="mate-terminal --command \"bash -c 'python3 " + filepath + "';bash\" --geometry +300+200 &"
    comment=filepath + "を実行します．"
    text_editor.insert("1.0", comment)
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    res = pro.stderr + '\n'
    text_editor.insert("1.0", res)

def remote_execute():
    filepath=entry_prog.get()
    program=re.findall('[^/]+.py',filepath, flags=re.IGNORECASE)[0]
        # filepath の末尾 .py から遡って最初の/までの文字列を抜き出す．
    cmd="rcp " + filepath + " robot@" + addr.get() + ": &"
    comment="\n "+program + "をリモートにコピーします．"
    text_editor.insert("1.0", comment)
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    res = pro.stderr + '\n'
    text_editor.insert("1.0", res)

    cmd="mate-terminal --command \"bash -c 'rsh robot@" + addr.get() + " micropython " + program + "';bash\" --geometry +300+200 &"
    comment="\n"+program + "をリモート実行します．"
    text_editor.insert("1.0", comment)
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    res = pro.stderr + '\n'
    text_editor.insert("1.0", res)

def show_addr():
    global addr
    print(addr.get())
    text_editor.insert(tk.END, "\n"+addr.get()+"\n")

def ping():
    global addr
    cmd="mate-terminal --command \"bash -c 'ping -c 7 " + addr.get() + "';bash\" --geometry +300+200 &"
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    text_editor.insert("1.0", addr.get() + "との接続を確認します．\n\n")

def open_ssh():
    global addr
    cmd="mate-terminal --command \"bash -c 'ssh robot@" + addr.get() + "';bash\" --geometry +300+200 &"
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    text_editor.insert("1.0", "\n" + cmd + "\n")
    text_editor.insert("1.0", addr.get() + "にsshでログインします．\n")
    text_editor.insert("1.0", "デフォルトパスワードは \"maker \"です．\n")

def ssh_key():
    global addr
    cmd="mate-terminal --command \"bash -c 'ssh-keygen -t rsa -f ~/.ssh/" + addr.get() + "_rsa';bash\" --geometry +300+200 &"
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    text_editor.insert("1.0", "\n" + cmd + "\n")

    cmd="mate-terminal --command \"bash -c 'ssh-copy-id -i ~/.ssh/" + addr.get() + "_rsa.pub robot@" + addr.get() + "';bash\" --geometry +300+200 &"
    pro = subprocess.run(cmd,shell=True ,capture_output=True,text=True)
    text_editor.insert("1.0", "\n" + cmd + "\n")

root = tk.Tk()
root.title('Robot Command Center 2 (RCC2)')

#root.rowconfigure(0, minsize=500, weight=1)
#root.columnconfigure(1, minsize=500, weight=1)

frame_button = tk.Frame(root, relief=tk.RAISED, bd=2, bg="orange")
frame_input = tk.Frame(root, relief=tk.RAISED, bd=2, bg="pink", width=1000)
frame_text = tk.Frame(root, relief=tk.RAISED, bd=2, bg="white", width=1000)

frame_input.grid(row=0, column=0,sticky="ns")
frame_text.grid(row=1, column=0,sticky="ns")
frame_button.grid(rowspan=2, row=0, column=1,sticky="ns")

text_editor = tk.Text(frame_text,bg="LightSkyBlue1",fg="black")
text_editor.grid(row=0, column=0, sticky="ew")
comment="これはリモートホスト(robot)で MicroPython を実行する ためのGUIです．\n"
text_editor.insert(tk.END, comment)
comment="IP Address: にリモートホストのアドレスを入力してください．\n"
text_editor.insert(tk.END, comment)

# Program Entry
filepath=StringVar()
filepath=''
Label=tk.Label(frame_input,text="Program: ",bg="white",fg="black")
entry_prog=tk.Entry(frame_input, textvariable=filepath, width=55,bg="IndianRed",fg="white")
entry_prog.insert(0,filepath)
Label.grid(row=0, column=0, sticky="ew")
entry_prog.grid(row=0, column=1, sticky=tk.EW)
button_new = tk.Button(frame_input, text='New', command=new_file, bg="RoyalBlue",fg="white")
button_new.grid(row=0, column=2, sticky="ew")

# IP Address Entry
addr=StringVar()
filepath='Untitled.py'
Label=tk.Label(frame_input,text="IP Address: ",bg="white",fg="black")
Addr=tk.Entry(frame_input, textvariable=addr, width=30,bg="IndianRed",fg="white")
Addr.insert(0,'172.16.7.xxx')
Label.grid(row=1, column=0, sticky="ew")
Addr.grid(row=1, column=1, sticky="ew")
button_ok = tk.Button(frame_input, text='ping', command=ping)
button_ok.grid(row=1, column=2, sticky="ew")


button_save = tk.Button(frame_button, text="Save As", padx=5, command=file_save)
button_gedit = tk.Button(frame_button, fg="white", bg="RoyalBlue", text="開く", padx=5, command=gedit)
button_gvim = tk.Button(frame_button, text="gvim", padx=5, command=open_gvim)
button_exec = tk.Button(frame_button, text="ローカル実行", padx=5, command=execute)
button_rexec = tk.Button(frame_button, bg="RoyalBlue3", fg="white", text="リモート実行", padx=5, height=2, command=remote_execute)
button_ssh = tk.Button(frame_button, text="ssh", padx=5, command=open_ssh)
button_key = tk.Button(frame_button, text="ssh-key", padx=5, command=ssh_key)

#button_open.grid(row=0, column=0, sticky='ew', padx=5, pady=5)
#button_save.grid(row=1, column=0, sticky="ew", padx=5)
button_gedit.grid(row=0, column=0, sticky="ew", padx=5, pady=1)
button_rexec.grid(row=1, column=0, sticky="ew", padx=5, pady=5)
button_gvim.grid(row=2, column=0, sticky="ew", padx=5, pady=1)
button_exec.grid(row=3, column=0, sticky="ew", padx=5, pady=1)
button_ssh.grid(row=4, column=0, sticky="ew", padx=5, pady=1)
button_key.grid(row=5, column=0, sticky="ew", padx=5, pady=1)



root.mainloop()
